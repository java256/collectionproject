package com.accenture.lkm.collection.activity.list;

import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) { // completed
		// Create an ArrayList to store names of employees.

		ArrayList<String> employeeName = new ArrayList<String>();
		employeeName.add("Pawan");
		employeeName.add("Ram");
		employeeName.add("Shyam");
		employeeName.add("Lakshman");
		employeeName.add("Sita");
		employeeName.add("Hanuman");
		// Print the List

		for (String s : employeeName) {
			System.out.println(s);
		}
	}
}