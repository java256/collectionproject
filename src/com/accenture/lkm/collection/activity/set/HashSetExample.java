package com.accenture.lkm.collection.activity.set;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetExample {

	public static void main(String[] args) {
		// Create a set to store account numbers
		
		HashSet<Integer> accountNumber = new HashSet<Integer>();
		accountNumber.add(454544545);
		accountNumber.add(454544546);
		accountNumber.add(454544544);
		accountNumber.add(4545445);
		accountNumber.add(454544549);
		// Print the set
		
		Iterator<Integer> iterator = accountNumber.iterator();
		while (iterator.hasNext()) {
			int accNumber = iterator.next();
			System.out.println(accNumber);
		}
	}
}