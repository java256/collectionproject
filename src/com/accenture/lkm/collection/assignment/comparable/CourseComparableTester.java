package com.accenture.lkm.collection.assignment.comparable;

import java.util.ArrayList;

public class CourseComparableTester {

	public static void main(String[] args) {

		// TODO

		// Create courses and add them to the ArrayList

		// Call print method of service class

		ArrayList<Course> courseList = new ArrayList<Course>();
		courseList.add(new Course(1, "BTECH", 25, 25000));
		courseList.add(new Course(2, "BCOM", 45, 12000));
		courseList.add(new Course(3, "MTECH", 85, 25000));
		courseList.add(new Course(4, "DOC", 25, 45000));
		courseList.add(new Course(5, "AGRI", 25, 89000));

		new CourseService().printAllCoursesSorted(courseList);

	}
}
