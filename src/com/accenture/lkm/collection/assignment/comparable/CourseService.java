package com.accenture.lkm.collection.assignment.comparable;

import java.util.Collections;
import java.util.List;

public class CourseService {

	// Method which helps to print courses sorted based on course fee

	public void printAllCoursesSorted(List<Course> courses) {

		// TODO

		// Sort the collection by calling Collections.sort method

		// Print course name and fee using foreach method

		Collections.sort(courses);

		courses.forEach(cou -> System.out.println(cou.getCourseName() + " " + cou.getCourseFee()));

	}

}
