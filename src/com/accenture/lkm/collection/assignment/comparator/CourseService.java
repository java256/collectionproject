package com.accenture.lkm.collection.assignment.comparator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CourseService {

	// Method which helps to print courses sorted based on course fee

	public void printAllCoursesFeesSorted(List<Course> courses) {

		// TODO

		// Sort the collection by calling Collections.sort method by passing comparator
		// object

		// Print course name and fee using foreach method

		Comparator<Course> comp = (o1, o2) -> {
			if (o1.getCourseFee() > o2.getCourseFee())
				return 1;
			return -1;
		};

		Collections.sort(courses, comp);
		courses.forEach(cou -> System.out.println(cou.getCourseName() + " " + cou.getCourseFee()));
	}

	public void printAllCoursesNamesSorted(List<Course> courses) {

		// TODO

		// Sort the collection by calling Collections.sort method

		// Print course name and fee using foreach method

		// Sort the collection by calling Collections.sort method by passing comparator
		// object

		// Print course name and fee using foreach method
		
		Comparator<Course> comp = (o1, o2) -> {
			return o1.getCourseName().compareTo(o2.getCourseName());
		};
		
		Collections.sort(courses, comp);
		courses.forEach(cou -> System.out.println(cou.getCourseName() + " " + cou.getCourseFee()));

	}

}
