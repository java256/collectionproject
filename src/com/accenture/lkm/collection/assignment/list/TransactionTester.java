package com.accenture.lkm.collection.assignment.list;

import java.util.ArrayList;

public class TransactionTester {

public static void main(String[] args) {
		
		//TODO
		
		//Create an ArrayList to store transaction objects
		
		//Create transaction objects and add them to the ArrayList
		
		//Call print method of service class which prints transactions corresponding to the account number
		
		ArrayList<Transaction> transactionList = new ArrayList<Transaction>();
		transactionList.add(new Transaction("TRN1", 12234578, 89745125, 800));
		transactionList.add(new Transaction("TRN2", 12234589, 89745155, 800));
		transactionList.add(new Transaction("TRN3", 12234585, 89745145, 800));
		transactionList.add(new Transaction("TRN4", 12234589, 89745165, 800));
		transactionList.add(new Transaction("TRN5", 12234545, 89745175, 800));
		
		new TransactionService().printAllTransactions(transactionList, 12234589);
	}
}
