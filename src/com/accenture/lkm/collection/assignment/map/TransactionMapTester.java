package com.accenture.lkm.collection.assignment.map;

import java.util.ArrayList;
import java.util.*;

public class TransactionMapTester {

	public static void main(String[] args) {
		// TODO

		// Create a list to hold transaction

		// Add transaction objects to the list

		// Create a map to store transactionId and transaction object

		// Call print method

		ArrayList<Transaction> transactionList = new ArrayList<Transaction>();
		transactionList.add(new Transaction("TRN1", 12234578, 89745125, 800));
		transactionList.add(new Transaction("TRN2", 12234589, 89745155, 800));
		transactionList.add(new Transaction("TRN3", 12234585, 89745145, 800));
		transactionList.add(new Transaction("TRN4", 12234589, 89745165, 800));
		transactionList.add(new Transaction("TRN5", 12234545, 89745175, 800));

		HashMap<String, Transaction> transactionMap = new HashMap<>();

		transactionList.forEach(tran -> transactionMap.put(tran.getTransactionId(), tran));

		new TransactionService().printAllTransactions(transactionMap, 12234589);

	}
}
